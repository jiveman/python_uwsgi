# Backup YouTube

This docker container contains a python exectuble that downloads youtube videos in a feed file on a routine basis along with a small Flask Web UI to manage downloads.

The app simply uses crontab to scan for `youtube_feed.txt` files in your $DATA_DIR/output and downloads all videos in the supplied playlist URLs. It keeps track of the videos it's already downloaded by creating a download.txt file.  

1. Launch the container
2. Setup a new password
3. Login to the webapp http://localhost:5000
4. Setup a download

## Requirements
- python3
- everything in the requirements.txt (python3-pip)


## Quickstart

Run the following commands to bootstrap your environment

```bash
    docker run -p 5000:5000 -e CRON_SCHEDULE="59 12 * * *" -v ~/data:/data --name buyt jiveman/backup_youtube
    # raspberry pi jiveman/backup_youtube_pi
```

The default **password** is `changeme`. Please change this by running the `/app/configure.py` script interactively as shown below.

| Env Variable | Function |
|---|---|
| DATA_DIR | path to data dir (where persistent data is stored) |
| DEBUG | Debug starts the entrypoint script with -x, and sets python web-UI to debug=True |
| LOG_DIR | Log directory, defaults to $DATA_DIR/logs |
| OUTPUT_DIR | Directory to output videos to. Defaults to $DATA_DIR/output |


## Shell

To open the interactive shell and configure a new password & settings, run

```bash
    docker exec -it buyt bash
    /app/configure.py
```

## uwsgi settings

To restart the webapp simply touch `/app/app.cfg`
