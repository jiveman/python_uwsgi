$(document).ready(function(){


	var prefetch_tags = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: {
			url: '/_get_tags?',
			cache: true
		},
		remote: {
			url: '/_get_tags/%QUERY',
			wildcard: '%QUERY'
		},
		limit: 10,
		minLength: 3
	});

	// var prefetch_countries = new Bloodhound({
	// 	datumTokenizer: Bloodhound.tokenizers.whitespace,
	// 	queryTokenizer: Bloodhound.tokenizers.whitespace,
	// 	local: ['python', 'perl'],
	// 	prefetch: {
	// 		url: 'https://raw.githubusercontent.com/twitter/typeahead.js/gh-pages/data/countries.json',
	// 		cache: true
	// 	}
	// });

	//prefetch_tags.clearPrefetchCache();
	prefetch_tags.initialize();

	var remote_tags = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/_get_tags/%QUERY',
			wildcard: '%QUERY'
		}
	});

	var remote_slugs = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/_get_slugs/%QUERY',
			wildcard: '%QUERY'
		}
	});

	// ** multiple data sources **
	// $('.prefetch input').typeahead({
	//   hint: true,
	//   minLength: 1,
	//   highlight: true
	// },
	// {
	//   name: 'tags',
	//   source: prefetch_tags
	// },
	// {
	//   name: 'tags',
	//   source: remote_tags
	// }
	// );
	$('.prefetch input').typeahead({
	  hint: true,
	  minLength: 1,
	  highlight: true
	},
	{
	  name: 'tags',
      source: prefetch_tags
	});

	// $('.search #post_search').typeahead({
	//   hint: true,
	//   minLength: 3,
	//   highlight: true
	// },
	// {
	//   name: 'posts',
	//   source: remote_slugs
	// });
});

// function get_youtube_video_details = {
// 	var xhr = new XMLHttpRequest();
//     xhr.open('GET', "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=<video ID>&fields=items/snippet/title,items/snippet/description&key=<API key>");
//     xhr.send();
//     xhr.onreadystatechange = function () {
//             if (xhr.readyState == 4 && xhr.status == 200) {
//                     var response = JSON.parse(xhr.responseText);
//                  // console.log("Description:", response.items[0].snippet.description);
//                //  var text = response.items[0].snippet.description;
//                    var text = "Find me at sand also at fgdyh  thrj hrtyrt fhrtgh";
//                    var html = findUrls(text);
//                    function findUrls(text) {
//                      var source = (text || '').toString();
//                      var urlArray = [];
//                      var url;
//                      var matchArray;
//                     // Regular expression to find FTP, HTTP(S) and email URLs.
//                      var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;
//                     // Iterate through any URLs in the text.
//                      while ((matchArray = regexToken.exec(source)) !== null) {
//                         var token = matchArray[0];
//                         urlArray.push(token);
//                     }
// }
