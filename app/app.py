import datetime
import functools
import os
from sys import exit
import re
# import urllib # old python2
from urllib.parse import urlencode, quote_plus
import configparser


from flask import (Flask, flash, Markup, redirect, render_template, request,
                   Response, session, url_for, jsonify)
from flask_bcrypt import Bcrypt
from loguru import logger

from markdown import markdown
from markdown.extensions.codehilite import CodeHiliteExtension
from markdown.extensions.extra import ExtraExtension
from micawber import bootstrap_basic, parse_html
from micawber.cache import Cache as OEmbedCache
from peewee import *
from playhouse.flask_utils import FlaskDB, get_object_or_404, object_list
from playhouse.sqlite_ext import *

# import ipdb
DATA_DIR = os.environ['DATA_DIR']
env_debug = os.environ['DEBUG']
if env_debug.lower() == 'true':
    env_debug = True
else:
    env_debug = False
log_dir = os.environ['LOG_DIR']
if not log_dir:
    log_dir = '/data/logs'
output_dir = os.environ['OUTPUT_DIR']
if not log_dir:
    log_dir = '/data/logs'
logger.remove()
logger.add(sys.stdout, colorize=True, format="[<cyan>{time:MM-DD HH:mm:ss}</cyan>] -  <level>{message}</level>", level="INFO")
logger.add("{}/app.log".format(log_dir), backtrace=True, format="{time:MM-DD HH:mm:ss}[{level}] {message}", rotation="5 MB")


bcrypt = Bcrypt()
BCRYPT_LOG_ROUNDS = 15
TITLE='YouTube Video Backup'
DOMAIN='example.com'

# Blog configuration values.

# You may consider using a one-way hash to generate the password, and then
# use the hash again in the login view to perform the comparison. This is just
# for simplicity.
APP_DIR = os.path.dirname(os.path.realpath(__file__))

# The secret key is used internally by Flask to encrypt session data stored
# in cookies. Make this unique for your app.
# Secrete key gets overridden with manage.py script

# This is used by micawber, which will attempt to generate rich media
# embedded objects with maxwidth=800.
SITE_WIDTH = 800

config = configparser.ConfigParser()
app_cfg = os.path.join(APP_DIR, 'app.cfg')
if (os.path.exists(app_cfg)):
    logger.info('loading conf {}'.format(app_cfg))
    config.read(app_cfg)
    if 'settings' not in config.sections():
        logger.error("Please run 'setup.py' to setup required settings.")
        exit(2)
    if 'comments' not in config.sections():
        logger.info("no comments section detected")
    required_settings = ['secret_key', 'password', 'title', 'domain' ]
    settings_in_config = config['settings'].keys()
    missing_req = set(required_settings) - set(settings_in_config)
    if missing_req:
        raise ValueError("Required config settings not found: {}".format(', '.join(missing_req)))
    ADMIN_PASSWORD = config['settings']['password']
    TITLE = config['settings']['title']
    DOMAIN = config['settings']['domain']
    SECRET_KEY = config['settings']['secret_key']
    BASE_DIR = config['settings']['base_download_dir']
    logger.debug("setting site title: {}".format(TITLE))
    logger.debug("setting site domain: {}".format(DOMAIN))
else:
    logger.error("Please run 'configure.py' to setup webapp")
    exit(2)

# The playhouse.flask_utils.FlaskDB object accepts database URL configuration.
DATABASE = 'sqliteext:///{}'.format(os.path.join(DATA_DIR, 'site.db'))
DEBUG = env_debug


# Create a Flask WSGI app and configure it using values from the module.
app = Flask(__name__)
app.config.from_object(__name__)

# ipdb.set_trace()

# FlaskDB is a wrapper for a peewee database that sets up pre/post-request
# hooks for managing database connections.
flask_db = FlaskDB(app)

# The `database` is the actual peewee database, as opposed to flask_db which is
# the wrapper.
database = flask_db.database

# Configure micawber with the default OEmbed providers (YouTube, Flickr, etc).
# We'll use a simple in-memory cache so that multiple requests for the same
# video don't require multiple network requests.
oembed_providers = bootstrap_basic(OEmbedCache())



def scan_output_dirs(rootDirs=[BASE_DIR], skip_spaces=True):
    all_dirs = list()
    for rootDir in rootDirs:
        for dirName, subdirList, fileList in os.walk(rootDir):
            if skip_spaces:
                subdirList = [ x for x in subdirList if ' ' not in x ]
                if ' ' in dirName:
                    continue
            print('Adding directory: {}'.format(dirName))
            all_dirs.append(dirName)
    return all_dirs

class Entry(flask_db.Model):
    title = CharField()
    url = CharField()
    path = CharField()
    slug = CharField(unique=True)
    urls = TextField()
    enabled = BooleanField(index=True)
    timestamp = DateTimeField(default=datetime.datetime.now, index=True)

    def __repr__(self):
        return '{0}: {1} - ({2})'.format(self.__class__, self.id, self.slug)
    def __str__(self):
        return str(self.__repr__())


    def save(self, *args, **kwargs):

        # Generate a URL-friendly representation of the entry's title.
        if not self.slug:
            self.slug = re.sub('[^\w]+', '_', self.title.lower()).strip('-')
        if ' ' in self.path:
            logger.info('found spaces in path, replacing none alpha chars with _')
            self.path = re.sub('[^\w]+', '_', self.path).strip('-')
        ret = super(Entry, self).save(*args, **kwargs)

        # Store search urls.
        self.update_search_index()
        self.update_download_config()
        return ret

    def update_download_config(self):
        ''' creates output dirs if they dont exist and creates youtube_feed.txt file for the scanner to download '''
        out_path = os.path.join(output_dir, self.path)
        feed_file = os.path.join(output_dir, self.path, 'youtube_feed.txt')
        if self.enabled != True:
            if os.path.exists(feed_file):
                logger.info("Deleting feed file for {}".format(self.title))
                os.remove(feed_file)
                return

        if not os.path.exists(out_path):
            logger.info('Making dir {}'.format(out_path))
            os.makedirs(out_path)
        # create feed file
        try:
            with open(feed_file, 'w') as fh:
                fh.write("{}\n".format(self.urls))
            logger.opt(ansi = True).info('<green>wrote {}</green>{}'.format(feed_file, self.urls))
        except Exception as err:
            logger.opt(ansi = True).info('<red>failed writing {}</red> {}'.format(feed_file, err))
            raise
        # return True
        

    def update_search_index(self):
        # Create a row in the FTSEntry table with the post urls. This will
        # allow us to use SQLite's awesome full-text search extension to
        # search our entries.
        exists = (FTSEntry
                  .select(FTSEntry.docid)
                  .where(FTSEntry.docid == self.id)
                  .exists())
        urls = '\n'.join((self.title, self.urls))
        if exists:
            (FTSEntry
             .update({FTSEntry.urls: urls})
             .where(FTSEntry.docid == self.id)
             .execute())
        else:
            FTSEntry.insert({
                FTSEntry.docid: self.id,
                FTSEntry.urls: urls}).execute()

    @classmethod
    def public(cls):
        return Entry.select().where(Entry.enabled == True)

    @classmethod
    def disabled(cls):
        return Entry.select().where(Entry.enabled == False)

    @classmethod
    def search(cls, query):
        words = [word.strip() for word in query.split() if word.strip()]
        if not words:
            # Return an empty query.
            return Entry.noop()
        else:
            search = ' '.join(words)

        # Query the full-text search index for entries matching the given
        # search query, then join the actual Entry data on the matching
        # search result.
        return (Entry
                .select(Entry, FTSEntry.rank().alias('score'))
                .join(FTSEntry, on=(Entry.id == FTSEntry.docid))
                .where(
                    FTSEntry.match(search) &
                    (Entry.enabled == True))
                .order_by(SQL('score')))


@app.route('/_get_slugs/<search>')
def get_slugs(search):
    if search:
        logger.info('searching for {}'.format(search))
        return jsonify([ x.slug for x in Entry.select().where(Entry.urls.contains(search)) ])

# @app.route('/_get_tags')
# @app.route('/_get_tags/<search>')
# def get_tags(search=None):
#     if search:
#         logger.info('searching for {}'.format(search))
#         return jsonify([ x.tag for x in Tag.select().where(Tag.tag.contains(search)) ])
#     return jsonify([ x.tag for x in Tag.select() ])

# @app.route('/update_output_dirs')
# def update_output_dirs():
#     dirs = scan_output_dirs()
#     ipdb.set_trace()
#     return "updated output_dirs: {}".format(', '.join(dirs))

# class EntryTags(Model):
#     entry = ForeignKeyField(Entry)
#     tag = ForeignKeyField(Tag)

class FTSEntry(FTSModel):
    urls = TextField()

    class Meta:
        database = database

def login_required(fn):
    @functools.wraps(fn)
    def inner(*args, **kwargs):
        if session.get('logged_in'):
            return fn(*args, **kwargs)
        return redirect(url_for('login', next=request.path))
    return inner

@app.route('/login/', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next') or request.form.get('next')
    if request.method == 'POST' and request.form.get('password'):
        password = request.form.get('password')
        # TODO: If using a one-way hash, you would also hash the user-submitted
        # password and do the comparison on the hashed versions.
        # if password == app.config['ADMIN_PASSWORD']:
        if bcrypt.check_password_hash(app.config['ADMIN_PASSWORD'], password):
            session['logged_in'] = True
            session.permanent = True  # Use cookie to store session.
            flash('You are now logged in.', 'success')
            return redirect(next_url or url_for('index'))
        else:
            flash('Incorrect password.', 'danger')
            logger.info('Incorrect password {} attempted'.format(password))
    return render_template('login.html', next_url=next_url, site_title=TITLE, domain=DOMAIN)

@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    if request.method == 'POST':
        session.clear()
        return redirect(url_for('login'))
    return render_template('logout.html', domain=DOMAIN, site_title=TITLE)

@app.route('/')
def index():
    search_query = request.args.get('q')
    if search_query:
        query = Entry.search(search_query)
    else:
        query = Entry.public().order_by(Entry.timestamp.desc())

    # The `object_list` helper will take a base query and then handle
    # paginating the results if there are more than 20. For more info see
    # the docs:
    # http://docs.peewee-orm.com/en/latest/peewee/playhouse.html#object_list
    return object_list(
        'index.html',
        query,
        search=search_query,
        check_bounds=False, domain=DOMAIN, site_title=TITLE)

def _create_or_edit(entry, template):
    if request.method == 'POST':
        entry.url = request.form.get('url') or ''
        entry.path = request.form.get('path') or ''
        entry.title = request.form.get('title') or ''
        entry.urls = request.form.get('urls') or ''
        entry.enabled = request.form.get('enabled') or False
        if not (entry.title and entry.urls):
            flash('Title, Download path and Urls are required.', 'danger')
        else:
            # Wrap the call to save in a transaction so we can roll it back
            # cleanly in the event of an integrity error.
            try:
                with database.atomic():
                    entry.save()
            except IntegrityError:
                flash('Error: this title is already in use.', 'danger')
            else:
                flash('Entry saved successfully.', 'success')
                if entry.enabled:
                    return redirect(url_for('detail', slug=entry.slug))
                else:
                    return redirect(url_for('edit', slug=entry.slug))

    return render_template(template, entry=entry, domain=DOMAIN, site_title=TITLE)

@app.route('/create/', methods=['GET', 'POST'])
@login_required
def create():
    return _create_or_edit(Entry(title='', urls='', path='', url='', output_dir=output_dir), 'create.html')

@app.route('/disabled/')
@login_required
def disabled():
    query = Entry.disabled().order_by(Entry.timestamp.desc())
    return object_list('index.html', query, check_bounds=False, domain=DOMAIN, site_title=TITLE)

@app.route('/<slug>/')
def detail(slug):
    if session.get('logged_in'):
        query = Entry.select()
    else:
        query = Entry.public()
    entry = get_object_or_404(query, Entry.slug == slug)
    return render_template('detail.html', entry=entry, domain=DOMAIN, site_title=TITLE, base_download_dir=BASE_DIR)

@app.route('/<slug>/edit/', methods=['GET', 'POST'])
@login_required
def edit(slug):
    entry = get_object_or_404(Entry, Entry.slug == slug)
    return _create_or_edit(entry, 'edit.html')

@app.template_filter('clean_querystring')
def clean_querystring(request_args, *keys_to_remove, **new_values):
    # We'll use this template filter in the pagination include. This filter
    # will take the current URL and allow us to preserve the arguments in the
    # querystring while replacing any that we need to overwrite. For instance
    # if your URL is /?q=search+query&page=2 and we want to preserve the search
    # term but make a link to page 3, this filter will allow us to do that.
    querystring = dict((key, value) for key, value in request_args.items())
    for key in keys_to_remove:
        querystring.pop(key, None)
    querystring.update(new_values)
    return urlencode(querystring)

@app.errorhandler(404)
def not_found(exc):
    return Response('<h3>Not found</h3>'), 404

def main():
    database.create_tables([Entry, FTSEntry], safe=True)
    app.run(debug=env_debug)

if __name__ == '__main__':
    print('running main')
    main()
