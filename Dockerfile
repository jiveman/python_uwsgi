FROM python:3.6-alpine

LABEL maintainer="jaime@carranza.co"

ENV APP_USER='www-data' \
    DATA_DIR='/data' \
    LOG_DIR='/data/logs' \
    DEBUG='false' \
    OUTPUT_DIR='/data/output'

RUN apk --no-cache add -U gcc linux-headers musl-dev bash libffi-dev curl ffmpeg \
    && pip install uwsgi 
    

#apk del gcc linux-headers musl-dev

# Create a group and user
RUN addgroup -S www-data && adduser -S www-data -G www-data

# Tell docker that all future commands should run as the appuser user
# USER www-data # Commented b/c uwsgi.ini runs as www-data

ADD app /app 

COPY requirements.txt /app/requirements.txt
COPY uwsgi.ini /etc/app/uwsgi.ini

# RUN pip install -r /etc/app/requirements.txt && touch /app/touch_me_to_reload_uwsgi && which uwsgi && uwsgi --version && which python && python --version

COPY entrypoint.sh /sbin/entrypoint.sh
COPY download_feeds.sh /usr/local/bin/download_feeds.sh

RUN chmod +x /usr/local/bin/download_feeds.sh

RUN chmod 755 /sbin/entrypoint.sh

RUN curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl && chmod a+rx /usr/local/bin/youtube-dl

# EXPOSE 80/tcp 5000/tcp 443/tcp
EXPOSE 5000/tcp

# WORKDIR '/'

ENTRYPOINT ["/sbin/entrypoint.sh"]

# CMD ["/usr/local/bin/uwsgi"]
# CMD ["/usr/local/bin/uwsgi", "--ini", "./uwsgi.ini"]