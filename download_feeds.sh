#!/bin/bash
set -e

if [ "[${DEBUG}]" = "[true]" ];then
  set -x
fi

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
  local var="$1"
  local fileVar="${var}_FILE"
  local def="${2:-}"
  if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
    echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
    exit 1
  fi
  local val="$def"
  if [ "${!var:-}" ]; then
    val="${!var}"
  elif [ "${!fileVar:-}" ]; then
    val="$(< "${!fileVar}")"
  fi
  export "$var"="$val"
  unset "$fileVar"
}


DATA_DIR=${DATA_DIR:-/data}
LOG_DIR=${LOG_DIR:-${DATA_DIR}/logs}
OUTPUT_DIR=${OUTPUT_DIR:-${DATA_DIR}/videos}
CACHE_DIR=${CACHE_DIR:-${DATA_DIR}/cache}

# APP_DATA_DIR=${DATA_DIR}/app

Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White
NC='\033[0m' # No Color

find $OUTPUT_DIR -type f -name "youtube_feed.txt"|while read filename
do
  printf "Found ${Green} $filename ${NC}\n"
  dirname=`dirname $filename`
  cd $dirnam
  basedir=`basename $dirname`
  cd $dirname
  echo "youtube-dl --playlist-reverse --download-archive ${dirname}/downloaded.txt -i -o \"%(playlist)s/%(uploader)s/%(uploader)s - S01E%(playlist_index)s - %(title)s [%(id)s].%(ext)s\" -f bestvideo[ext=mp4]+bestaudio[ext=m4a] --merge-output-format mp4 --add-metadata --write-thumbnail --batch-file=$filename"
  youtube-dl --playlist-reverse --download-archive ${dirname}/downloaded.txt -i -o "%(playlist)s/%(uploader)s/%(uploader)s - S01E%(playlist_index)s - %(title)s [%(id)s].%(ext)s" -f bestvideo[ext=mp4]+bestaudio[ext=m4a] --merge-output-format mp4 --add-metadata --write-thumbnail --batch-file=$filename
done


