#!/bin/bash
set -e


# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
  local var="$1"
  local fileVar="${var}_FILE"
  local def="${2:-}"
  if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
    echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
    exit 1
  fi
  local val="$def"
  if [ "${!var:-}" ]; then
    val="${!var}"
  elif [ "${!fileVar:-}" ]; then
    val="$(< "${!fileVar}")"
  fi
  export "$var"="$val"
  unset "$fileVar"
}

file_env 'UWSGI_INI'
file_env 'REQUIREMENTS'

UWSGI_INI=${UWSGI_INI:-/etc/app/uwsgi.ini}
REQUIREMENTS=${REQUIREMENTS:-/app/requirements.txt}

# ROOT_PASSWORD=${ROOT_PASSWORD:-password}

export DATA_DIR="${DATA_DIR:-/data}"
export LOG_DIR="${LOG_DIR:-${DATA_DIR}/logs}"
export OUTPUT_DIR="${OUTPUT_DIR:-${DATA_DIR}/videos}"
export CRON_SCHEDULE="${CRON_SCHEDULE:-'0 3 * * *'}"
CACHE_DIR="${CACHE_DIR:-${DATA_DIR}/cache}"
DEBUG=${DEBUG:-0}

if [ "[$DEBUG]" = "[true]" ];then
  set -x
fi
# APP_DATA_DIR=${DATA_DIR}/app

Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White
NC='\033[0m' # No Color

create_APP_data_dir() {
  # mkdir -p ${APP_DATA_DIR}

  # populate default APP configuration if it does not exist
  # if [ ! -d ${APP_DATA_DIR}/etc ]; then
    # mv /etc/app ${APP_DATA_DIR}/etc
  # fi
  # rm -rf /etc/app
  # ln -sf ${APP_DATA_DIR}/etc /etc/app
  # chmod -R 0775 ${APP_DATA_DIR}
  # chown -R ${APP_USER}:${APP_USER} ${APP_DATA_DIR}

  if [ ! -d ${OUTPUT_DIR} ]; then
    echo "Creating output dir [${OUTPUT_DIR}]"
    mkdir -p ${OUTPUT_DIR}
    chown ${APP_USER}:${APP_USER} ${OUTPUT_DIR}
  fi

  if [ ! -d ${LOG_DIR} ]; then
    echo "Creating log dir [${LOG_DIR}]"
    mkdir -p ${LOG_DIR}
    chown ${APP_USER}:${APP_USER} ${LOG_DIR}
  fi
  touch /data/touch_me_to_reload_uwsgi
  chown ${APP_USER}:${APP_USER} /data/touch_me_to_reload_uwsgi
  chgrp ${APP_USER} $DATA_DIR

  
}


create_pid_dir() {
  mkdir -p /var/run/uwsgi
  chmod 0775 /var/run/uwsgi
  chown root:${APP_USER} /var/run/uwsgi
}

create_APP_cache_dir() {
  if [ ! -d ${CACHE_DIR} ]; then
    echo "Creating cache dir [${CACHE_DIR}"
    mkdir -p ${CACHE_DIR}
    chmod 0775 ${CACHE_DIR}
    chown root:${APP_USER} ${CACHE_DIR}
  fi
}

first_init() {
  echo "first run"
  if [ ! -f /data/.initialized ]; then
    echo "installing requirements [$REQUIREMENTS]"
    pip install -r $REQUIREMENTS
    # echo "Removing unused system apps"
    # apk del gcc linux-headers musl-dev
    # python3 configure.py
    
    (crontab -l ; echo "$CRON_SCHEDULE /usr/local/bin/download_feeds.sh 2>&1" ) | sort - | uniq - | crontab -
    printf "To configure your web app run: ${Cyan}docker exec -it $HOSTNAME /app/configure.py${NC}"
    chown www-data /app
    if [ ! -f /data/site.db ];then
        cp /app/site.db /data/site.db
        chown www-data /data/site.db
    fi
    touch /data/.initialized
  fi
}

create_pid_dir
create_APP_data_dir
create_APP_cache_dir

# allow arguments to be passed to uwsgi
if [[ ${1:0:1} = '-' ]]; then
  EXTRA_ARGS="$*"
  set --
elif [[ ${1} == uwsgi || ${1} == "$(which uwsgi)" ]]; then
  EXTRA_ARGS="${*:2}"
  set --
fi

# default behaviour is to launch uwsgi
if [[ -z ${1} ]]; then
  first_init
  echo "Starting crond..."
  /usr/sbin/crond -f -L /dev/stdout &
  echo "Starting uwsgi..."
  # cd /app && exec python wsgi.py ${EXTRA_ARGS}
  exec "/usr/local/bin/uwsgi" --ini "${UWSGI_INI}" ${EXTRA_ARGS}
else
  echo "foo"
  exec "$@"
fi
